angular.module('app', ["ngRoute"])
    .config(function ($routeProvider) {
        $routeProvider.when('/home', {templateUrl: 'partials/home.html'});
        $routeProvider.when('/menu', {templateUrl: 'partials/menu.html'});
        $routeProvider.when('/cart', {templateUrl: 'partials/cart.html'});
        $routeProvider.otherwise({redirectTo: '/home'});
    }
);