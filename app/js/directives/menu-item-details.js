var app = angular.module('app');
app.directive('menuItemDetails', function() {
    return {
        restrict: 'A',
        scope: {
            item: '='
        },
        templateUrl: 'js/directives/menuItemDetails.html',
        controller: 'MenuItemController'
    }
});