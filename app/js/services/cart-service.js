var app = angular.module('app');

app.service('CartService', function () {
    var self = this;
    var items = {};

    self.addItemToCart = function(item) {
        if(item.id in items) {
            var storedItem = items[item.id];
            storedItem.amount = storedItem.amount + 1;
        } else {
            item.amount = 1;
            items[item.id] = item;
        }
    }

    self.getItems = function() {
        return items;
    }

    self.getNumberOfItems = function() {
        var sum = 0;
        for(var i in items) {
            sum += items[i].amount;
        }
        return sum;
    }

    return self;
});
