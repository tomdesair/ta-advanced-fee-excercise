var app = angular.module('app');

app.controller('MenuItemController', function ($scope, CartService) {

    $scope.addCurrentItem = function(item) {
        CartService.addItemToCart(item);
    }
});
