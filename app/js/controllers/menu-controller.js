var app = angular.module('app');

app.controller('MenuController', function ($scope, $http, CartService) {

    var getItems = function (url) {
        $http({method: 'GET', url: url}).
            success(function (data, status, headers, config) {
                $scope.menuTitle = data.title;
                $scope.menuItems = data._embedded.items;
                if (data._links.parent) {
                    $scope.parentUrl = data._links.parent.href;
                }
            }).
            error(function (data, status, headers, config) {
                $scope.menuTitle = "Unable to fetch data!";
            });
    }

    var getDescription = function (url) {
        $http({method: 'GET', url: url}).
            success(function (data, status, headers, config) {
                $scope.selectedItem = data;
            }).
            error(function (data, status, headers, config) {
                $scope.menuTitle = "Unable to fetch data!";
            });
    }

    getItems('http://localhost:1337/');

    $scope.title = "This is the menu of the AE Café";

    $scope.fetchDetails = function (item) {
        if (item._links.children) {
            getItems(item._links.children.href);
            $scope.selectedItem = undefined;
        } else {
            getDescription(item._links.self.href);
        }
    }

    $scope.goBackToParent = function (url) {
        getItems(url);
        $scope.selectedItem = undefined;
    }

    $scope.getNumberOfItemsInCart = function() {
        return CartService.getNumberOfItems();
    }
});
