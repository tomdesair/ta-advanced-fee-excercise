var app = angular.module('app');

app.controller('CartController', function ($scope, CartService) {
    $scope.getCartItems = function() {
        return CartService.getItems();
    }
});
